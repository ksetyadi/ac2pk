def get_pk(data):
	w, ln, h, pos, direction = data

	# convert meters to feet. 1 m = 3.33 feet
	w = w * 3.33
	ln = ln * 3.33
	h = h * 3.33

	i = 10 if pos == 'Bawah' else 18
	
	if direction == 'Utara':
		e = 16

	if direction == 'Timur':
		e = 17

	if direction == 'Selatan':
		e = 18

	if direction == 'Barat':
		e = 20

	btu = (w * ln * h * i * e) / 60

	return btu / 10000

def get_raw_input():
	w = float(raw_input('Masukkan panjang ruangan (m): '))
	ln = float(raw_input('Masukkan lebar ruangan (m): '))
	h = float(raw_input('Masukkan tinggi ruangan (m): '))
	pos = raw_input('Masukkan posisi ruangan [Atas, Bawah]: ')
	direction = raw_input('Masukkan arah ruangan [Timur, Selatan, Barat, Utara]: ')

	return [w, ln, h, pos, direction]


def main():
	data = get_raw_input()
	print 'Kebutuhan AC Anda adalah, {0}'.format(get_pk(data)) 

if __name__ == '__main__':
	main()